<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <title>
                Call  App
            </title>
            <!-- <link href="{{asset('dist/css/main.css')}}" rel="stylesheet">
                <link href="{{asset('/dist/css/bootstrap.css')}}" rel="stylesheet">
                </link>
            </link>
 -->        </meta>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<style type="text/css">
    nav ul li a {
        color: white!important;
    }
</style>
    </head>
    <body>
       
<!-- Grey with black text -->
<nav class="navbar navbar-expand-sm bg-dark navbar-light">
  <ul class="navbar-nav">
    <li class="nav-item active">
      <a class="nav-link" href="/">Home</a>
    </li>
    <li class="nav-item active" >
      <a class="nav-link" href="/event">All Events</a>
    </li>
  
  </ul>
</nav>
<br>
        <div class="container">
           
            <!-- /navbar -->
            <!-- Main component for call to action -->
            @yield('content')
        </div>
        <!-- /container -->
       
    </body>
</html>
