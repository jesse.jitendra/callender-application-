@extends('layouts.base')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-4">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
 Add Event
</button>

<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Create Events</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body border">
       
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4">
             <form action="{{route('cal.store')}}" method="POST" role="form">
        {{csrf_field()}}
        <center>  <legend>
            Create Event
        </legend></center>
        <div class="form-group">
            <label for="title">
                Title
            </label>
            <input class="form-control" name="title" placeholder="Title" type="text">
        </div>
        <div class="form-group">
            <label for="description">
                Description
            </label>
            <input class="form-control" name="description" placeholder="Description" type="text">
        </div>
        <div class="form-group">
            <label for="start_date">
                Start Date
            </label>
            <input class="form-control" name="start_date" placeholder="Start Date" type="text">
        </div>
        <div class="form-group">
            <label for="end_date">
                End Date
            </label>
            <input class="form-control" name="end_date" placeholder="End Date" type="text">
        </div>
        <div class="form-group">
          <label>Enter first Email</label>
          <input type="email" name="email1" class="form-control">
        </div>
        <div class="form-group">
          <label>Enter Second Email</label>
          <input type="email" name="email2" class="form-control">
        </div>
        <button class="btn btn-primary" type="submit">
            Submit
        </button>
        <br>
    </form>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4"></div>
    </div>
</div>
    
@endsection
