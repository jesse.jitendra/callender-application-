@extends('layouts.base')


@section('content')

<!DOCTYPE html>
<html>
<head>
<title></title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js"></script>
<style type="text/css">
	.sidenav {
  /*width: 130px;*/
  width: 100%!important;
  /*position: fixed;*/
  z-index: 1;
  top: 20px;
  left: 10px;
  background: #eee;
  overflow-x: hidden;
  padding: 8px 0;
}

.sidenav a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 15px;
  color: #2196F3;
  display: block;
}

.sidenav a:hover {
  color: #064579;
}

.main {
  margin-left: 140px; /* Same width as the sidebar + left position in px */
  font-size: 28px; /* Increased text to enable scrolling */
  padding: 0px 10px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
#title-txt
{
	text-align: center; 
	font-size: 20px!important;
	font-family: italic;
	font-weight: bold;
	text-transform: uppercase;
}
</style>
<script type="text/javascript">
	function addattendee()
	{
		
		var email1 = prompt('Enter first attendee email');
		var email2 = prompt('Enter Second attendee email');
		var email3 = prompt('Enter Second attendee email');
		var email4 = prompt('Enter Second attendee email');
		var email5 = prompt('Enter Second attendee email'); 
        if(email2 != '' )
        {
         var attendee_array = [email1, email2,email3,email3,email5];
		document.getElementById("attendee_arr").value = attendee_array;	
        }
        else{

        }
		
	}
</script>
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col">
			<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myModal">create Event</button>
			 <!-- <input type="button" name="abc" onclick="addattendee();" value="click"> -->

<!--  Modal Create new Events -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Create Events</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body border">
          <form action="{{route('cal.store')}}" method="POST" role="form">
        {{csrf_field()}}
        <center>  <legend>
            Create Event
        </legend></center>
        <div class="form-group">
            <label for="title">
                Title
            </label>
            <input class="form-control" name="title" placeholder="Title" type="text">
        </div>
        <div class="form-group">
            <label for="description">
                Description
            </label>
            <input class="form-control" name="description" placeholder="Description" type="text">
        </div>
        <div class="form-group">
            <label for="start_date">
                Start Date
            </label>
            <input class="form-control" name="start_date" placeholder="2015-05-28T09:00:00-07:00" type="text">
        </div>
        <div class="form-group">
            <label for="end_date">
                End Date
            </label>
            <input class="form-control" name="end_date" placeholder="2015-05-28T09:00:00-07:00" type="text">
        </div>
        <div class="form-group">
          <label>Enter attendee  Email</label>
          <input type="email" name="email1" onkeyup="addattendee()" class="form-control">
        </div>
        <input type="button" name="abc" onclick="addattendee();" class="btn btn-default" value="add attendees">
        

        <input type="hidden" name="attendee_arr" id="attendee_arr">
        <br><br>
        <button class="btn btn-primary" type="submit">
            Submit
        </button>
        <br>
          </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
	</div>
	<div class="col"></div>
	<div class="col"></div>
</div><!-- closing row -->

	<div class="row">
		<div class="col-3">
			<br><br>
			<div class="sidenav">
                <a href=""><b><i>Edit Events by clicking below </i></b></a>
                <?php $array = json_decode(json_encode($data),true); 
                    foreach ($array as $singleArray) {
                    	
                    	?>
                    	<div> </div>
<!--         <button type="button" class="btn " data-toggle="modal" data-target="#myModal<?= $singleArray['id'] ?>"> </button>  -->
        <p id="title-txt" ><i> <?= $singleArray['title'] ?> </i></p>
      
         <div class="d-flex">
         &emsp;&emsp;<a href="{{ url('/deleteevent',[$singleArray['id']] )  }} " style="color: white;" class="btn btn-info">Delete</a>&emsp;&emsp;&emsp;&emsp;
        <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#myModal<?= $singleArray['id'] ?>">edit </button></div>
        <br>

<!-- Model for updating event --->                         
<div class="modal" id="myModal<?= $singleArray['id'] ?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Events</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body border">
          <form action="/updateevent" method="POST" role="form">
        {{csrf_field()}}
        <center>  <legend>
            Create Event
        </legend></center>
        <input type="hidden" name="eventId" value="<?= $singleArray['id'] ?>">
        <div class="form-group">
            <label for="title">
                Title
            </label>
            <input class="form-control" name="title" placeholder="Title" type="text" value="<?= $singleArray['title'] ?>">
        </div>
        <div class="form-group">
            <label for="description">
                Description
            </label>
            <input class="form-control" name="description" placeholder="Description" type="text" value="<?= $singleArray['description'] ?>">
        </div>
        <div class="form-group">
            <label for="start_date">
                Start Date
            </label>
            <input class="form-control" name="start_date" placeholder="Start Date" type="text" value="<?= $singleArray['start'] ?>">
        </div>
        <div class="form-group">
            <label for="end_date">
                End Date
            </label>
            <input class="form-control" name="end_date" placeholder="End Date" type="text" value="<?= $singleArray['end'] ?>">
        </div>
        
        <button class="btn btn-primary" type="submit">
            Submit
        </button>
        <br>
    </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<?php
    }         
    ?>
    </div>
    </div>
	<div class="col-9">
		@if(Session::get('uopdatesuccess') != null )
			<div class="aleert alert-success">
				{{ Session::get('uopdatesuccess') }}
				<button class="btn btn-default" data-dismiss='alert'>x</button>
			</div>
			@endif
			<div id="calendar"></div>
			<?php $array = json_decode(json_encode($data),true); 
        
                    foreach ($array as $singleArray) {
                    }   
              ?>
              <?php ?>
		</div>
	</div>
</div>




<script type="text/javascript">
	$(document).ready(function() {
  // page is now ready, initialize the calendar...
  $('#calendar').fullCalendar({
 
    events: '{{ url("api/cal") }}'

  });
});
</script>

</body>
</html>

@endsection

